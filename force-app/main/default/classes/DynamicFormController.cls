public without sharing class DynamicFormController {
    public String test;
    public Integer i;
    
    public DynamicFormController(Integer i) {

    }

    public DynamicFormController(Integer i, String test){
        this.test = test;
        this.i = i;
    }


    @AuraEnabled (cacheable = true)
    public static String getObjectEditForm(String dynamicFormName, Integer parentFormNumber){
        List<DynamicForm__mdt> genericInfo = [SELECT ObjectType__c FROM DynamicForm__mdt 
                                              WHERE MasterLabel =: dynamicFormName 
                                              AND ParentFormNumber__c =: parentFormNumber LIMIT 1]; 
        if(!genericInfo.isEmpty()){
            System.debug('genericInfo : ' + genericInfo);
            return genericInfo.get(0).ObjectType__c;
        }
        return '';
    }

    @AuraEnabled (cacheable = true)
    public static List<String> getDefaultDisplayInputs(Integer parentFormNumber){
        System.debug('### getDefaultDisplayInputs ###');
        List<DynamicForm__mdt> dynamicFormMdt_lst = [SELECT InputApiName__c, InputType__c, OrderDisplay__c   FROM DynamicForm__mdt 
                                                       WHERE ParentFormNumber__c =: parentFormNumber AND DefaultDisplay__c = true
                                                       ORDER BY OrderDisplay__c ASC]; 
        return getInputsName(dynamicFormMdt_lst);
    }


    @AuraEnabled (cacheable = true)
    public static List<String> getStandardInputs(String objectType, Integer parentFormNumber, String fieldName, String fieldValue){
        System.debug('### getStandardInputs ###');
        System.debug('ObjectType : ' + objectType + ' - ' + 'FieldName : ' + fieldName);
        if(String.isNotBlank(objectType) && String.isNotBlank(fieldName)){
            if(getFieldType(objectType, fieldName) == 'PICKLIST'){
                String conditionalRendering = fieldName + '=' + '\'' + fieldValue + '\'';
            
                List<DynamicForm__mdt> dynamicFormMdt_lst = [SELECT InputApiName__c, InputType__c, OrderDisplay__c   FROM DynamicForm__mdt 
                                                            WHERE ParentFormNumber__c =: parentFormNumber AND ConditionalRendering__c =: conditionalRendering
                                                            ORDER BY OrderDisplay__c ASC];
                return getInputsName(dynamicFormMdt_lst);  
            }
        }
        
        return null;
                                                    
                                                       
        
    }

    public static List<String> getInputsName(List<DynamicForm__mdt> dynamicFormMdt_lst){
        System.debug('defaultDisplayInputs : ' + dynamicFormMdt_lst);                                               
        if(!dynamicFormMdt_lst.isEmpty()){
            List<String> inputApiName = new List<String> ();
            for(DynamicForm__mdt dynamicFormMdt : dynamicFormMdt_lst){

                if(String.isNotBlank(dynamicFormMdt.InputApiName__c)){
                    inputApiName.add(dynamicFormMdt.InputApiName__c);
                }
                
            }
            System.debug(inputApiName);
            return inputApiName;
        }

        return null;
    }

    public static String getFieldType(String objectType, String fieldName){
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(objectType).getDescribe().fields.getMap();
        Schema.SObjectField field = fieldMap.get(fieldName);
        Schema.DisplayType fieldType = field.getDescribe().getType();
        System.debug(fieldName + ' type : ' + String.valueOf(fieldType));
        return String.valueOf(fieldType);
    }
}
