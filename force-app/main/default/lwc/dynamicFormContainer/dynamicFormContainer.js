import { LightningElement, api, wire, track } from 'lwc';
import getObjectEditForm from '@salesforce/apex/DynamicFormController.getObjectEditForm';
import getDefaultDisplayInputs from '@salesforce/apex/DynamicFormController.getDefaultDisplayInputs';
import getStandardInputs from '@salesforce/apex/DynamicFormController.getStandardInputs';

export default class DynamicFormContainer extends LightningElement {

    @api parentFormNumber;
    @api dynamicFormName;

    editFormObject = '';
    inputApiName = [];
    @track currentFieldName;
    @track currentFieldValue;


    @wire(getStandardInputs, { objectType: '$editFormObject', parentFormNumber: '$parentFormNumber',
                               fieldName : '$currentFieldName', fieldValue : '$currentFieldValue'})
    wiredInputs({ error, data }) {
        if (data) {
            console.log('LA !');
            console.log(data);
        } else if (error) {
            this.error = error;
        }
    }

    connectedCallback(){
        console.log('Connected Callback : ' + this.dynamicFormName + ' / ' + this.parentFormNumber);
        getObjectEditForm({dynamicFormName : this.dynamicFormName, parentFormNumber : this.parentFormNumber})
            .then(result => {
                this.editFormObject = result;
                console.log(this.editFormObject);
                getDefaultDisplayInputs({parentFormNumber : this.parentFormNumber})
                    .then(res => {
                        this.inputApiName = res;
                        console.log(this.inputApiName);

                        
                    })
                    .catch(error => {
                        this.error = error;
                    });

            })
            .catch(error => {
                this.error = error;
            });
    }

    reloadFields(event){
        
        console.log('Reload fields');
        this.currentFieldName = event.target.fieldName;
        this.currentFieldValue = event.target.value;
        console.log(this.currentFieldName + ' : ' + this.currentFieldValue);

        var allInputs = this.template.querySelectorAll(".testInp");

        allInputs.forEach(function(element){
            console.log('Element ?' + element);
            console.log(element.name);
            if(element.name != this.currentFieldName){
                element.removeAttribute("onchange");
            }
        },this);

    }


}